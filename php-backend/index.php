<?php
header('Access-Control-Allow-Origin: *');

require 'Application/Controller/DB.class.php';
require 'Application/Controller/index.class.php';
require 'Application/Controller/Router.class.php';

/**
 * Movies Router
 */

Router::Run('/index.php/{id}', 'Home@index');
Router::Run('/index.php/allmovies', 'Home@allmovies');
Router::Run('/index.php/search/{value}', 'Home@search');
Router::Run('/index.php/movies_detail2/{value}', 'Home@MoviesDetail2');
Router::Run('/index.php/trend', 'Home@trend');
Router::Run('/index.php/benzer', 'Home@benzer');
Router::Run('/index.php/categories', 'Home@categori_list');
Router::Run('/index.php/categorie', 'Home@categori_list2');
Router::Run('/index.php/categori/{id}', 'Home@categori_detail');
Router::Run('/index.php/categori/search/{id}', 'Home@CategorySearch','POST');


/**
 * Series Router
 */
Router::Run('/index.php/series', 'Series@index');
Router::Run('/index.php/serie/{id}', 'Series@Serie');
Router::Run('/index.php/series_list/{id}', 'Series@series_list');
Router::Run('/index.php/series_detail/{id}', 'Series@series_detail');
Router::Run('/index.php/series/search/{value}', 'Series@Search');

/**
 * Imdb 
 */

Router::Run("/index.php/imdb/{value}", 'IMDB@index');

/**
 * Youtube
 */
Router::Run("/index.php/youtube", 'Youtube@index');
Router::Run("/index.php/youtube/search/{value}", 'Youtube@search');
Router::Run("/index.php/youtube/detail/{value}", 'Youtube@detail');
Router::Run("/index.php/youtube/trend", 'Youtube@trend');



