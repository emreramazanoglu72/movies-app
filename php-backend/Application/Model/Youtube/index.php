<?php




class Youtube extends Controller
{
    public $API_KEY = "";
    public $API_URL = "https://www.googleapis.com/youtube/v3/search?&part=snippet&maxResults=50&key=AIzaSyBEgNFrlhA7hBsnUrSnk-hwKFRMH6uNUNw";
    public $API_URL_VIDEO = "https://www.googleapis.com/youtube/v3/videos?&part=snippet&maxResults=50&key=AIzaSyBEgNFrlhA7hBsnUrSnk-hwKFRMH6uNUNw";

    public function index()
    {
        $arr = ['Route ' => [
            [
                'info' => 'Youtube Key Search',
                'path' => '/youtube/search/{value}'
            ],
            [
                'info' => 'Youtube Video Detail',
                'path' => '/youtube/detail/{value}'
            ],
            [
                'info' => 'Youtube Trend List',
                'path' => '/youtube/trend'
            ]
        ]];
        print_r(json_encode($arr));
    }

    public function search($key)
    {
        $key = $key[1];
        echo $this->Curl($this->API_URL . "&q={$key}");
    }

    public function detail($key)
    {
        $key = $key[1];
        echo $this->Curl($this->API_URL_VIDEO . "&id={$key}");
    }

    public function trend()
    {
        echo $this->Curl('https://www.googleapis.com/youtube/v3/videos?part=snippet&chart=mostPopular&regionCode=tr&maxResults=15&key=AIzaSyBEgNFrlhA7hBsnUrSnk-hwKFRMH6uNUNw');
    }
}
