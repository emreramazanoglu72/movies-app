<?php


class Series extends Controller
{
    /**
     * Series List Service Function
     */
    public function index()
    {
        global $db;

        $query = $db->from('system_series')
            ->all();
        print_r(json_encode($query));
    }

    /**
     * Series List Service Function
     */
    public function Serie($id)
    {
        global $db;
        $id = $id[1];

        $query = $db->from('system_series')
            ->where('series_id', $id)
            ->first();
        print_r(json_encode($query));
    }
         public function getVideo($id){
        $content = json_decode(file_get_contents("https://emreramazanoglu.com/youtube/video_info.php?id={$id}"));
        foreach($content as $item):
            if($item->format == "mp4, video, 720p, audio"){
                return $item->url;
            }else if($item->format == "mp4, video, 360p, audio"){
                return $item->url;
            }else{
                return false;
            }
        endforeach;
    }

    /**
     * Series List Service Function
     */
    public function series_detail($id)
    {
        global $db;
    
        $id = $id[1];

        $query = $db->from('series_list')
            ->where('movie_id', $id)
            ->first();
        $youtube = json_decode( file_get_contents("https://emreramazanoglu.com/film/index.php/bot/youtube_dl/{$query['movies_ytid']}"),true);
        $arr = [];
        $youtube["video_url"] = $this->getVideo($query['movies_ytid']);
     print_r(json_encode($youtube));
    }


    /**
     * Sezon List Service Function
     */
    public function series_list($id)
    {

        global $db;
        $limit = $_POST['page_id'];
        if($limit){
               
          
            $totalRecord = $db->from('series_list')
            ->select('count(movie_id) as total')
            ->total();
        $pageLimit = 10;
        $pageParam = 'page';
        $pagination = $db->pagination($totalRecord, $pageLimit, $limit);
        // normal sorgumuz
        $query = $db->from('series_list')
            ->where('movies_series', $id[1])

            ->limit($pagination['start'], $pagination['limit'])
            ->all();
        print_r(json_encode($query));
        }else{
             $query = $db->from('series_list')
            ->where('movies_series', $id[1])
            ->all();
                    print_r(json_encode($query));

        }
    }

    /**
     * Sezon List Service Function
     */
    public function series_search($id)
    {
        global $db;
        $list = [];
        $kelime = $_POST['kelime'];
        $sorgu = $db->query("SELECT * FROM series_list WHERE movie_title LIKE  '%{$kelime}%'  and movies_series = '{$id[1]}'", PDO::FETCH_ASSOC);
        foreach ($sorgu as $pos) {
            array_push($list, $pos);
        }
        print_r(json_encode($list));
    }
}
