<?php


class Home extends Controller
{
    /**
     * Movies List Service Function
     */
    public function index($id)
    {
        global $db;
        $limit = $id[1];
        $totalRecord = $db->from('system_movies')
            ->where('count(movies_id) as total')
            ->select('movie_active', 1)
            ->total();
        // sayfa başına kaç veri gözükecek?
        $pageLimit = 20;
        // sayfa parametresi? Örn: index.php?page=2 [page = $pageParam]
        $pageParam = 'page';
        // limit için start ve limit değerleri hesaplanıyor
        $pagination = $db->pagination($totalRecord, $pageLimit, $limit);
        // normal sorgumuz
        $query = $db->from('system_movies')
            ->orderby('movies_views', 'DESC')
            ->limit($pagination['start'], $pagination['limit'])
            ->all();
        print_r(json_encode($query));
    }

    public function getVideo($id){
        $content = json_decode(file_get_contents("https://emreramazanoglu.com/youtube/video_info.php?id={$id}"));
        foreach($content as $item):
            if($item->format == "mp4, video, 720p, audio"){
                return $item->url;
            }else if($item->format == "mp4, video, 360p, audio"){
                return $item->url;
            }else{
                return false;
            }
        endforeach;
    }
    public function MoviesDetail2($args)
    {
        global $db;

        $seflink = $args[1];
        $kon = $db->query("SELECT * FROM system_movies where movies_seflink = '{$seflink}'")->fetch(PDO::FETCH_ASSOC);
        if ($kon == []) {
            print_r(json_encode(['status' => 'boş']));
        } else {
            $student = $db->from('system_movies')->where('movies_seflink', $seflink)->first();

            $views = $student['movies_views'] + 5;
            $query = $db->update('system_movies')
                ->where('movies_id', $student['movies_id'])
                ->set(array(
                    'movies_views' => $views
                ));
            $student["movies_url"] = $this->getVideo($seflink);
            print_r(json_encode($student));
        }
    }


 
    /**
     * Movies Search Service Function
     */
    public function search($key)
    {
        global $db;
        $list = [];
        $kelime = $key[1];
        $sorgu = $db->query("SELECT * FROM system_movies WHERE movies_title LIKE  '%{$kelime}%'", PDO::FETCH_ASSOC);
        foreach ($sorgu as $pos) {
            array_push($list, $pos);
        }
        print_r(json_encode($list));
    }

    /**
     * Movies Search Service Function
     */
    public function CategorySearch($key)
    {


        global $db;
        $list = [];
        $key = $key[1];
        //  print_r($_POST);
        $sorgu = $db->query("SELECT * FROM system_movies  WHERE movies_title LIKE  '%{$_POST['keyword']}%'", PDO::FETCH_ASSOC);


        foreach ($sorgu as $cate) {
            //  print_r($cate);
            $cates = $cate['movies_category'];
            $cates = explode(',', $cates);
            // print_r($cates);
            foreach ($cates as $cat) {
                if ($key == $cat) {
                    array_push($list, $cate);
                    // print_r($cat);
                }
            }
        }
        print_r(json_encode($list));
    }

    /**
     * Trend Movie Service Function
     */
    public function trend()
    {
        global $db;
        $list =  $db->from('system_movies')
            ->where('movie_active', 1)
            ->orderby('movies_views', 'DESC')
            ->limit("0", '15')
            ->all();
        print_r(json_encode($list));
    }



    /**
     * Trend Movie Service Function
     */
    public function benzer()
    {
        global $db;
        $list =  $db->from('system_movies')
            ->where('movie_active', 1)
            ->orderby('movies_views', 'DESC')
            ->limit("0", '5')
            ->all();
        print_r(json_encode($list));
    }

    /**
     * All Movies Function
     */
    public function allmovies()
    {
        global $db;
        $allmovies = $db->from('system_movies')->where('movie_active', 1)->orderby('movies_id', 'DESC')->all();

        print_r(json_encode($allmovies));
    }


     /**
     * Categori List Function
     */
    public function categori_list2()
    {
        global $db;
        $arr = [];
        $allmovies = $db->from('system_categories')->all();
        foreach($allmovies as $item):
            $list = [];
            $detail = $db->from("system_movies")->where("movies_category",$item["categori_id"])->all();
            foreach($detail as $lists):
                array_push($list,$lists);
            endforeach;

            array_push($arr,["info"=>$item,"list"=>$list]);
        endforeach;
        print_r(json_encode($arr));
    }

    /**
     * Categori Detail Function
     */
    public function categori_detail($id)
    {
        global $db;
        $id = $id[1];
        $allmovies = $db->from('system_movies')->where('movie_active', 1)->all();
        $arr = [];
        foreach ($allmovies as $cate) {

            $cates = $cate['movies_category'];
            $cates = explode(',', $cates);
            // print_r($cates);
            foreach ($cates as $cat) {
                if ($id == $cat) {
                    array_push($arr, $cate);
                    // print_r($cat);
                }
            }
        }

        print_r(json_encode($arr));
    }


}
