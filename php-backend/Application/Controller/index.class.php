<?php 

const DB_HOST = "localhost";

const DB_NAME = "dbname";

const DB_USER = "dbuser";

const DB_PASSWORD = "dbpass";

$db = new fastdb(DB_HOST,DB_NAME,DB_USER,DB_PASSWORD);

class Controller {
    
    function __construct() {
    }
    
    public static function Curl($url){
        return file_get_contents($url); 
    }
    
    public function Parse($content,$parse)
    {
        preg_match_all('@'.$parse.'@',$content,$response);
        return $response;
    }
    
    public function Component($component,$array = null)
    {
        
        require "Application/Views/Components/{$component}.php";
        
        if(is_array($array)){
            
            print_r(is_array($array));
            @call_user_func($component,$array);
        }
    }
    
    public function render($render,$call = null)
    {
        
        if(!file_exists("Application/Views/{$render}/index.php")){
            
            require "Application/Views/Error/index.php";
            die();
        }else{
            $call = $call;
            require "Application/Views/{$render}/index.php";
        }
    }
    
    public function  JSON ($content){
        return  print_r(json_encode($content));
    }
    
  
    
    
}



?>