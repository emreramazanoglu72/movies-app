import Link from 'next/link'
import React, { useState } from 'react'

const movies = (props) => {
    return (
        <div className="general">
            <div className="container">
                <div className="bs-example bs-example-tabs" role="tabpanel" data-example-id="togglable-tabs">
                  
                    <div id="myTabContent" className="tab-content">


                        {props.detail.map((item,key) => <div key={key} className="col-md-2 w3l-movie-gride-agile">
                            <Link href={'/series/detail/[seflink]'}
                                as={`/series/detail/${item.movie_id}`} prefetch={false} >
                                <a className="hvr-shutter-out-horizontal">
                                    <img src={item.movies_image} style={{ height: 250 }} title="album-name" className="img-responsive" alt=" " />
                                    <div className="w3l-action-icon"><i className="fa fa-play-circle" aria-hidden="true" />
                                    </div>
                                </a>

                            </Link>
                            <div className="mid-1 agileits_w3layouts_mid_1_home">
                                <div className="w3l-movie-text">
                                    <h6><Link href={`/series/detail/${item.movie_id}`}><a>{item.movie_title}</a></Link></h6>
                                </div>
                                <div className="mid-2 agile_mid_2_home">
                                    <p>2016</p>
                                    <div className="block-stars">
                                        <ul className="w3l-ratings">
                                            <li><a href="#"><i className="fa fa-star" aria-hidden="true" /></a></li>
                                            <li><a href="#"><i className="fa fa-star" aria-hidden="true" /></a></li>
                                            <li><a href="#"><i className="fa fa-star" aria-hidden="true" /></a></li>
                                            <li><a href="#"><i className="fa fa-star" aria-hidden="true" /></a></li>
                                            <li><a href="#"><i className="fa fa-star-half-o" aria-hidden="true" /></a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div className="clearfix" />
                                </div>
                            </div>
                            <div className="ribben">
                                <p>NEW</p>
                            </div>
                        </div>)}


                        <div className="clearfix"> </div>


                    </div>
                </div>
            </div>
        </div>
    )
}

export default movies;
