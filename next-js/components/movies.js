import Link from 'next/link'
import React, { useState } from 'react'
import { useRouter } from 'next/router'
import MoviesCard from './moviesCard'
import PlayCircleFilled from '@material-ui/icons/PlayCircleFilled';

import { Button } from '@material-ui/core'
const movies = (props) => {
    const [limit, setLimit] = useState(15);
    return (
        <div className="general">
            <h4 className="latest-text w3_latest_text">Filmler</h4>
            <div style={{ marginTop: 25, display: 'flex', flexDirection: 'row', flexWrap: 'wrap', padding: 25 }}>
                {props.categorie.slice(0, limit).map((item,key) =><div key={key} style={{ width: 350, borderRadius: 15, margin: 5, boxShadow: "0px 7px 20px rgba(141, 40, 173, 0.15)" }}>
                      
                 <img style={{ height: 450, width: 350, borderRadius: 15 }} src={item.movies_images} alt="" />
                        <div style={{ padding: 15 }}>


                            <h1 style={{ fontSize: 30, fontWeight: 'bold', textAlign: 'center' }}>{item.movies_title}</h1>
                            <p style={{ color: 'gray', alignSelf: 'center' }}>
                                The aging patriarch of an organized crime dynasty transfers control of his clandestine empire to his reluctant son.
                                </p>
                            <div style={{ textAlign: 'center' }}>
                                <Link href={'/movies/[key]'}
                                    as={`/movies/${item.movies_seflink}`} prefetch={false}>
                                    <Button
                                        style={{
                                            marginLeft: 150,
                                            alignSelf: 'center',
                                            borderRadius: 30,
                                            margin: 5,
                                            height: 46,
                                            backgroundColor: '#F5C518',
                                            "boxShadow": "0px 7px 20px rgba(141, 40, 173, 0.15)"
                                        }}
                                        variant="contained"
                                        color="primary"
                                        size="large"
                                        startIcon={<PlayCircleFilled />}
                                    >
                                        Hemen İzle
                                    </Button>
                                </Link>
                            </div>
                        </div>

                    </div>
                )}


                <div className="clearfix"> </div>


            </div>
            {Object.keys(props.categorie).length < limit ? null : <div style={{ textAlign: 'center' }} >
                <Button onClick={() => setLimit(limit + 12)} variant="outlined" color="primary">
                    Daha Fazla göster
                    </Button>
            </div>}
        </div>

    )
}
movies.getInitialProps = async (ctx) => {
    const res = await fetch('http://emreramazanoglu.com/film/index.php/series')
    const json = await res.json()
    return { trend: json, categorie: json2 }
}

export default movies;
