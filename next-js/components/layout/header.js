import Link from 'next/link';
import React, { useState } from 'react'
import { useRouter } from 'next/router'

const header = () => {
    const router = useRouter();
    var [search, setSearch] = useState('');
    const changetext = (e) => {
        setSearch(e.target.value);
    }

    const getSearch = () => {
        // router.push("/search/[slug]", `/search/${search}`);

    }
    return (
        <div>
            <div className="header">
                <div className="container">
                    <div className="w3layouts_logo">
                        <Link href="/">
                            <a href="/">
                                <h1>Movies<span style={{ color: 'orange' }}>Club</span></h1>
                            </a>
                        </Link>

                    </div>
                    <div className="w3_search">
                        <input type="text" onChange={changetext} name="Search" placeholder="ara" required />
                        <Link href={'/search/[key]'} as={`/search/${search}`}><input type="submit" onClick={getSearch} defaultValue="Ara" /></Link>
                    </div>

                    <div className="clearfix"> </div>
                </div>
            </div>
            <div className="movies_nav">
                <div className="container">
                    <nav className="navbar navbar-default">
                        <div className="navbar-header navbar-left">
                            <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                <span className="sr-only">Toggle navigation</span>
                                <span className="icon-bar" />
                                <span className="icon-bar" />
                                <span className="icon-bar" />
                            </button>
                        </div>
                        <div className="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
                            <nav>
                                <ul className="nav navbar-nav">
                                    <li className="active">
                                        <Link href="/">
                                            <a href="/">Ana Sayfa</a>
                                        </Link>
                                    </li>
                                    <li>
                                        <Link href={'/series'}>
                                            <a href="/series">Diziler</a>
                                        </Link>
                                    </li>
                                    <li>
                                        <Link href={'/favority'}>
                                            <a href="/favority">Favoriler</a>
                                        </Link>
                                    </li>
                                    <li>
                                        <Link href={'/imdb'}>
                                            <a href="/imdb">IMDB</a>
                                        </Link>
                                    </li>

                                </ul>
                            </nav>
                        </div>
                    </nav>
                </div>
            </div>

        </div>


    )
}

export default header;