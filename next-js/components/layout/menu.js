import React from 'react'

const menu = () => {
    return (
        <div className="menu">
            <ul>
                <li><a className="active" href="index.html"><i className="home" /></a></li>
                <li><a href="videos.html">
                    <div className="video"><i className="videos" /><i className="videos1" /></div>
                </a></li>
                <li><a href="reviews.html">
                    <div className="cat"><i className="watching" /><i className="watching1" /></div>
                </a></li>
                <li><a href="404.html">
                    <div className="bk"><i className="booking" /><i className="booking1" /></div>
                </a></li>
                <li><a href="contact.html">
                    <div className="cnt"><i className="contact" /><i className="contact1" /></div>
                </a></li>
            </ul>
        </div>

    )
}

export default menu;