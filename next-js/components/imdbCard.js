import Button from '@material-ui/core/Button';
import FavoriteIcon from '@material-ui/icons/Favorite';
import SearchIcon from '@material-ui/icons/Search';
import Link from 'next/link';
import React, { useEffect, useState } from 'react';
const imdbCard = (props) => {
    var [fav, setFav] = useState(false);
    /**
    * Add Favority function
    */
    const addFavority = () => {
        var data = JSON.parse(localStorage.getItem('IMDB'));
        data.push(props);
        localStorage.setItem('IMDB', JSON.stringify(data));
        setFav(true);
    }
    const removeFavority = () => {
        var data = JSON.parse(localStorage.getItem('IMDB'));
        var arr = [];
        data.map((item, key) => {
            if (item.id != props.id) {
                arr.push(item)
            }
        })
        localStorage.setItem('IMDB', JSON.stringify(arr));
        setFav(false);
    }
    const FavoryityControl = () => {
        var data = JSON.parse(localStorage.getItem('IMDB'));
        data.map(item => {
            if (item.id == props.id) {
                setFav(true)
            }
        })
    }
    useEffect(()=>{FavoryityControl()},[])
    return (
        <div style={{
            marginTop: 60,
            marginBottom: 60,
            background: "#FAFAFA",
            borderRadius: "10px",
            display: 'flex',
            backgroundColor: 'white',
            padding: 10,
            boxShadow: "0px 7px 20px rgba(141, 40, 173, 0.15)"

        }}>
            <div>
                <img style={{
                    borderRadius: 30,
                    marginRight: 20,
                    height: 400,
                    width: 350
                }} src={`http://image.tmdb.org/t/p/w500/${props.poster_path}`} />
            </div>
            <div>
                <div style={{
                    display: 'flex'
                }}>
                    <img src="/images/imdb.png" style={{ height: 40, alignSelf: 'center' }} alt="" />
                    <h3 style={{ alignSelf: 'center', marginLeft: 5 }}>{props.vote_average}</h3>
                    <div style={{ marginLeft: 320 }}>
                        <Button
                            style={{
                                borderRadius: 30,
                                height: 46,
                                margin: 5,
                                backgroundColor: '#F5C518',
                                boxShadow: "0px 7px 20px rgba(141, 40, 173, 0.15)"
                            }}
                            variant="contained"
                            color="primary"
                            size="large"
                            startIcon={<SearchIcon />}
                        >
                            Aksiyon
                    </Button>
                        <Button
                            style={{
                                borderRadius: 30,
                                margin: 5,
                                height: 46,
                                backgroundColor: '#F5C518',
                                "boxShadow": "0px 7px 20px rgba(141, 40, 173, 0.15)"
                            }}
                            variant="contained"
                            color="primary"
                            size="large"
                            startIcon={<SearchIcon />}
                        >
                            Biografi
                    </Button>
                    </div>
                </div>
                <div style={{ marginTop: 50 }}>
                    <h2 style={{ color: '#F5C518' }}>
                        {props.release_date.split('-')[0]}
                    </h2>
                    <h1 style={{ fontWeight: 'bold', fontSize: 50 }}>{props.title}</h1>
                    <p style={{ "width": "471px", "height": "64px", "left": "calc(50% - 471px/2 + 146.5px)", "top": "1309px", "fontFamily": "Poppins", "fontStyle": "normal", "fontWeight": "normal", "fontSize": "16px", "lineHeight": "35px", "color": "rgba(0, 0, 0, 0.5)" }}>
                        {props.overview.substr(0, 150)}...
                    </p>
                    <div style={{ marginTop: 50 }}>
                        <Button
                            onClick={fav ? removeFavority : addFavority}
                            color={"secondary" }
                            style={{
                                borderRadius: 30,
                                margin: 5,
                                height: 46,
                                backgroundColor: '#F5C518',
                                "boxShadow": "0px 7px 20px rgba(141, 40, 173, 0.15)"
                            }}
                            variant="contained"
                            size="large"
                            startIcon={<FavoriteIcon />}
                        >
                            {fav ? "Favorilerden Çıkar" : "Favorile Ekle"}
                    </Button>
                        <Link href={`/imdb/${props.id}`}>
                            <a href={'#'} style={{ marginLeft: 25, color: '#F5C518' }}>Daha fazla göster -></a>
                        </Link>
                    </div>
                </div>
            </div>
        </div>
    )
}


export default imdbCard
