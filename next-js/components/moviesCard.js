import React from 'react'

const moviesCard = (props) => {
    return (
            <div className="card movie_card w3l-movie-gride-agile">
                <img src="https://www.joblo.com/assets/images/joblo/posters/2018/11/Spider-Verse-poster-1.jpg" className="card-img-top" alt="..." />
                <div className="card-body">
                    <i className="fas fa-play play_button" data-toggle="tooltip" data-placement="bottom" title="Play Trailer">
                    </i>
                    <h5 className="card-title">Spider-Man: Into the Spider-Verse</h5>
                    <span className="movie_info">2019</span>
                    <span className="movie_info float-right"><i className="fas fa-star" /> 9 / 10</span>
                </div>
            </div>

              
    )
}

export default moviesCard
