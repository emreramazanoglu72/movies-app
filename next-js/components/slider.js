// import React from 'react'
import Carousel from 'react-multi-carousel';

import React, { useEffect, useState } from 'react'
import { getTrendMovies } from '../controller/service';

const Slider = (props) => {
  const [trend, setTrand] = useState([1, 2, 3]);
  useEffect(() => {
    setTrand(props.trend);
  }, []);
  const responsive = {
    superLargeDesktop: {
      // the naming can be any, depends on you.
      breakpoint: { max: 4000, min: 3000 },
      items: 5
    },
    desktop: {
      breakpoint: { max: 3000, min: 1024 },
      items: 3
    },
    tablet: {
      breakpoint: { max: 1024, min: 464 },
      items: 2
    },
    mobile: {
      breakpoint: { max: 464, min: 0 },
      items: 1
    }
  };
  return (
    <section className="w3l-main-slider position-relative" id="home">
      <div className="companies20-content">
        <div className="owl-one owl-carousel owl-theme">
          <Carousel responsive={responsive}>
            {trend.slice(0,8).map(item => <div className="item">
              <li>
                <div className="slider-info banner-view " style={{ width:750,background: `url(${item.series_image}) no-repeat` }}>
                  <div className="banner-info">
                    <h3>{item.series_title}</h3>
                    {/* <p dangerouslySetInnerHTML={{__html: item.series_body.substr(0, 150)}}></p> */}
                    <a href="#small-dialog1" className="popup-with-zoom-anim play-view1">
                      <span className="video-play-icon">
                        <span className="fa fa-play" />
                      </span>
                      <h6>hemen izle</h6>
                    </a>
                    {/* dialog itself, mfp-hide class is required to make dialog hidden */}
                    <div id="small-dialog1" className="zoom-anim-dialog mfp-hide">
                      <iframe src="https://player.vimeo.com/video/358205676" allow="autoplay; fullscreen" allowFullScreen />
                    </div>
                  </div>
                </div>
              </li>
            </div>)}
          </Carousel>
        </div>
      </div>
    </section>

  )
}
export default Slider;
// const Slider = () => {

//   return (
//     <div style={{
//       padding: 20,
//       backgroundColor: '#dfb636'
//     }}>
//       <Carousel responsive={responsive}>
//         {[1, 2, 3, 4, 1, 2, 3, 4, 1, 2, 3, 4, 1, 2, 3, 4].map(item =><img style={{margin:5}} src={`images/r${item}.jpg`} alt />
//          )}


//       </Carousel>
//     </div>

//   )
// }

// export default Slider;