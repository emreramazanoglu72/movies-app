const BASE_API_URL = "http://emreramazanoglu.com/film/index.php";
const get = (path) => {
   return window.fetch(path)
        .then(res=>res.json())
        .then(res=>res)
        .catch(err=>console.log(err));
}

const post = (path) => {
    return  window.fetch(BASE_API_URL,path)
        .then(res=>res.json)
        .then(res=>res)
        .catch(err=>console.log(err));
}

export {get,post}