import { get } from '../api'
const searchIMDB = (key) => {
    return get(`https://api.themoviedb.org/3/search/movie?api_key=844dba0bfd8f3a4f3799f6130ef9e335&language=tr-TR&query=${key}`).then(res => res);
}

export { searchIMDB }