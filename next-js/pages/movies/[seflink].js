import Link from 'next/link';
import React, { useEffect, useState } from 'react'
import Header from '../../components/layout/header';
import { Player } from 'video-react';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/FavoriteSharp';
import Head from 'next/head';
const Movies = (props) => {
    var [data, setData] = useState([]);
    var [play, setPlay] = useState(false);
    var [fav, setFav] = useState(false);
    const addFavority = () => {
        var data = JSON.parse(localStorage.getItem('favority'));
        data.push(props.detail);
        localStorage.setItem('favority', JSON.stringify(data));
        setFav(true);
    }
    const removeFavority = () => {
        var data = JSON.parse(localStorage.getItem('favority'));
        var arr = [];
        data.map((item, key) => {
            if (item.movies_id != props.detail.movies_id) {
                arr.push(item)
            }
        })
        localStorage.setItem('favority', JSON.stringify(arr));
        setFav(false);
    }
    const FavoryityControl = () => {
        var data = JSON.parse(localStorage.getItem('favority'));
        data.map(item => {
            if (item.movies_id == props.detail.movies_id) {
                setFav(true)
            }
        })
    }
    useEffect(() => {
        setFav(false);

        setPlay(false);
        FavoryityControl();

        setData(props.detail)
    }, [props.detail]);

    return (
        <>
            <Head>
                <title>{data.movies_title}</title>
            </Head>
            <Header />
            <div className="single-page-agile-main">
                <div className="container">
                    {/* /w3l-medile-movies-grids */}
                    <div className="agileits-single-top">
                        <ol className="breadcrumb">
                            <li>
                                <Link href="/">
                                    <a href="/">Ana Sayfa</a>
                                </Link>
                            </li>
                            <li>filmler</li>
                            <li className="active">{data.movies_title}</li>
                        </ol>
                    </div>
                    <div className="single-page-agile-info">
                        {/* /movie-browse-agile */}
                        <div className="show-top-grids-w3lagile">
                            <div className="col-sm-8 single-left">
                                <div className="song">
                                    <div className="song-info" style={{ display: 'flex', flexDirection: 'row' }}>
                                        <h3 style={{ flex: 1 }}>{data.movies_title}</h3>
                                        <IconButton onClick={fav ? removeFavority : addFavority} color={fav ? "secondary" : "default"} aria-label="delete">
                                            <DeleteIcon color={'green'} />
                                        </IconButton>
                                    </div>
                                    <div className="video-grid-single-page-agileits">
                                        {play == true ? <Player

                                            videoId="video-1"
                                        >
                                            <source src={data.movies_url} />
                                        </Player> : <div style={{ position: 'relative' }}>
                                                <img style={{ height: 500 }} src={data.movies_images} />
                                                <button>
                                                    <img onClick={() => {
                                                        setPlay(true)
                                                    }} src="http://www.slatecube.com/images/play-btn.png" style={{ position: 'absolute', left: 25, top: 25, height: 85, width: 85 }} />
                                                </button>
                                            </div>}

                                    </div>
                                    <p style={{ fontSize: 16, fontWeight: 'bold' }} dangerouslySetInnerHTML={{ __html: data.movies_content }} ></p>
                                </div>

                                <div className="clearfix"> </div>

                            </div>
                            <div className="col-md-4 single-right">
                                <h3>Up Next</h3>
                                <div className="single-grid-right">

                                    {props.smilar.map((item,key) => <div key={key} className="single-right-grids">
                                        <div className="">
                                            <Link href={'/movies/[key]'}
                                                as={`/movies/${item.movies_seflink}`} prefetch={false}><a ><img src={item.movies_images} style={{ height: 200 }} alt /></a></Link>
                                        </div>
                                        <div className="">
                                            <Link href={'/movies/[key]'}
                                                as={`/movies/${item.movies_seflink}`} prefetch={false}><a className="title"> {item.movies_title}</a></Link>
                                            <p className="author"><a href="#" className="author">John Maniya</a></p>
                                            <p className="views">2,114,200 views</p>
                                        </div>
                                        <div className="clearfix"> </div>
                                    </div>)}


                                </div>
                            </div>
                            <div className="clearfix"> </div>
                        </div>

                    </div>
                </div>
            </div>
        </>

    )
}
Movies.getInitialProps = async (ctx) => {
    const res = await fetch(`http://emreramazanoglu.com/film/index.php/movies_detail2/${ctx.query.seflink}`)
    const json = await res.json()

    const smilar = await fetch(`http://emreramazanoglu.com/film/index.php/benzer`)
    const json2 = await smilar.json()
    return { detail: json, smilar: json2 }
}

export default Movies;