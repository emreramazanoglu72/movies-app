import Head from 'next/head';
import React from 'react';
import Header from '../components/layout/header';
import Movies from '../components/movies';

/**
 * Render Function Component for dashBoard
 * @param {*} props 
 */
const Dashboard = (props) => {
  /**
   * Return Method
   */
  return (
    <>
      <Head>
        <title>Ana Sayfa</title>
      </Head>
      <Header />
     <Movies {...props}/>
    </>
  )
}

/**
 * SSR Fetch Function 
 * @param {*} ctx 
 */
Dashboard.getInitialProps = async (ctx) => {

  const data = await fetch('http://emreramazanoglu.com/film/index.php/allmovies')
  const json = await data.json()
  return { categorie: json }
}


export default Dashboard;