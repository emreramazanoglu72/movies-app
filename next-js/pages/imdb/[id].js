
import Head from 'next/head';
import Link from 'next/link';
import React from 'react';
import Header from '../../components/layout/header';
/**
 * Material UI Library import
 */
import Button from '@material-ui/core/Button';
import SearchIcon from '@material-ui/icons/Search';
/**
 * IMDB Detail Screen
 * @param {*} props 
 */
const IMDBDetail = (props) => {
    return (
        <div>
            <Head>
                <title>{props.info.title}</title>
            </Head>
            <Header />
            <div style={{
                backgroundImage: 'url(/images/background.jpg)',
                padding: 20,
                alignSelf: 'center',
                height: 85,
                display: 'flex'
            }}>
                <Link href="/">
                    <a href="/">
                        <h2 style={{ marginLeft: 250, color: '#F5C518' }}>Home /</h2>
                    </a>    
                </Link>
                <h2 style={{ marginLeft: 10 }}>{props.info.title}</h2>
            </div>
            <div className="container">
                <div style={{
                    marginTop: 60,
                    marginBottom: 60,
                    background: "#FAFAFA",
                    borderRadius: "10px",
                    display: 'flex',
                    backgroundColor: 'white',
                    padding: 10,
                    boxShadow: "0px 7px 20px rgba(141, 40, 173, 0.15)"

                }}>
                    <div>
                        <img style={{
                            borderRadius: 30,
                            marginRight: 20,
                            width: 350
                        }} src={`http://image.tmdb.org/t/p/w500/${props.info.poster_path}`} />
                    </div>
                    <div>
                        <div style={{
                            display: 'flex'
                        }}>
                            <img src="/images/imdb.png" style={{ height: 40, alignSelf: 'center' }} alt="" />
                            <h3 style={{ alignSelf: 'center', marginLeft: 5 }}>{props.info.vote_average}</h3>
                            <div style={{ marginLeft: 320 }}>
                                <Button
                                    style={{
                                        borderRadius: 30,
                                        height: 46,
                                        margin: 5,
                                        backgroundColor: '#F5C518',
                                        boxShadow: "0px 7px 20px rgba(141, 40, 173, 0.15)"
                                    }}
                                    variant="contained"
                                    color="primary"
                                    size="large"
                                    startIcon={<SearchIcon />}
                                >
                                    Aksiyon
                    </Button>
                                <Button
                                    style={{
                                        borderRadius: 30,
                                        margin: 5,
                                        height: 46,
                                        backgroundColor: '#F5C518',
                                        "boxShadow": "0px 7px 20px rgba(141, 40, 173, 0.15)"
                                    }}
                                    variant="contained"
                                    color="primary"
                                    size="large"
                                    startIcon={<SearchIcon />}
                                >
                                    Biografi
                    </Button>
                            </div>
                        </div>
                        <div style={{ marginTop: 50 }}>
                            <h2 style={{ color: '#F5C518' }}>
                                {props.info.release_date.split('-')[0]}
                            </h2>
                            <h1 style={{ fontWeight: 'bold', fontSize: 50 }}>{props.info.title}</h1>
                            <p style={
                                {
                                    fontSize: 25,
                                    color: "rgba(0, 0, 0, 0.5)"
                                }}>
                                {props.info.overview}
                            </p>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
/**
 * SSR Fetch Function 
 */
IMDBDetail.getInitialProps = async (req) => {
    /**
     * Get Movies Detail Request 
     * req => get request query id
     */
    const res = await fetch(`https://api.themoviedb.org/3/movie/${req.query.id}?api_key=844dba0bfd8f3a4f3799f6130ef9e335&language=tr-TR`)
    /**
     * return props
     */
    const json = await res.json()
    return { info: json }
}

export default IMDBDetail
