import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import SearchIcon from '@material-ui/icons/Search';
import React, { useEffect, useState } from 'react';
import ImdbCard from '../../components/imdbCard';
import Header from '../../components/layout/header';
import { searchIMDB } from '../../controller/service';
import Head from 'next/head';

const IMDB = (props) => {
    /**
     * Set Component State
     */
    var [data, setData] = useState([]);
    var [key, setKey] = useState('');
    var [type, setType] = useState('movies');
    var [year, setYear] = useState('2020');
    /**
     * get props data to state
     */
    useEffect(() => {
        setData(props.best.results)
    }, []);
    /**
     * Search Function
     */
    const Search = () => {

        searchIMDB(key, type, year)
            .then(item => setData(item.results));
    }
   
    /**
     * return Component
     */
    return (
        <div>
            <Head>
                <title>IMDB Search</title>
            </Head>
            <Header />
            <div style={{
                height: "701px",
                width: '100%',
                alignSelf: 'center',
                backgroundImage: 'url(/images/bg.jpg)',
                backgroundRepeat: 'no-repeat',
                backgroundSize: 'cover',
                backgroundPosition: 'center center',
            }}>

                <div style={{ backgroundColor: 'rgba(245, 197, 24, 0.608)', height: '100%' }}>
                    <div style={{ left: 361 }}>
                        <h2 style={{
                            color: 'white',
                            padding: 100,
                            fontFamily: "Poppins",
                            fontSize: "65px",
                            fontStyle: "italic",
                            fontWeight: "bold",
                            lineHeight: "84px",
                            letterSpacing: "0em",
                            textAlign: "left",
                            left: '361px'
                        }
                        }>
                            Welcome to MovieUP.
                </h2>
                        <p style={{
                            color: 'rgba(255,255,255,0.800000011920929)',
                            fontSize: 25,
                            marginLeft: 90,
                            width: 1000
                        }}>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris
                </p>
                    </div>
                    <div
                        style={{
                            position: 'absolute',
                            top: 770,
                            left: 400,
                            background: '#fff',
                            border: '1px solid #ffff',
                            borderRadius: 35,
                            boxShadow: "0px 16px 28px rgba(141, 40, 173, 0.15)"
                        }}>
                        <div style={{ padding: 25, display: 'flex', flexDirection: 'row', alignSelf: 'center' }}>
                            <div className="form-group" style={{ margin: 10 }}>
                                <select className="form-control" style={{
                                    backgroundColor: '#F3F3F3',
                                    borderRadius: 8,
                                    width: "161px",
                                    height: "68px",
                                    color: '#636363'
                                }}
                                    onChange={(e) => setYear(e.target.value)}
                                    defaultValue={'Yıl'}>
                                    <option value={2020}>2020</option>
                                    <option value={2019}>2019</option>
                                    <option value={2018}>2018</option>
                                    <option value={2017}>2017</option>
                                    <option value={2016}>2016</option>
                                    <option value={2015}>2015</option>
                                    <option value={2014}>2014</option>
                                    <option value={2013}>2013</option>
                                    <option value={2012}>2012</option>
                                    <option value={2011}>2011</option>
                                    <option value={2010}>2010</option>
                                </select>
                            </div>
                            <div className="form-group" style={{ margin: 10 }}>
                                <select className="form-control" style={{
                                    backgroundColor: '#F3F3F3',
                                    borderRadius: 8,
                                    width: "161px",
                                    height: "68px",

                                }}
                                    onChange={(e) => setType(e.target.value)}
                                >
                                    <option type={'movie'}>Film</option>
                                    <option type={'series'}>Dizi</option>
                                    <option type={'episode'}>episode</option>
                                </select>
                            </div>
                            <div className="form-group" style={{ margin: 10, position: 'relative', display: 'flex', flexDirection: 'row' }}>
                                <input
                                    onChange={(e) => setKey(e.target.value)}
                                    placeholder={'Enter movie name here'}
                                    style={{
                                        width: "554px",
                                        flex: 1,
                                        height: "68px",
                                        background: "#F3F3F3",
                                    }} type="text"
                                    className="form-control" />
                                <IconButton style={{ position: 'absolute', right: 0, marginTop: 10 }} type="submit" aria-label="search">
                                    <SearchIcon />
                                </IconButton>
                            </div>

                            <Button
                               
                                style={{
                                    borderRadius: 30,
                                    margin: 10,
                                    height: 68,
                                    backgroundColor: '#F5C518',
                                    "boxShadow": "0px 7px 20px rgba(141, 40, 173, 0.15)"
                                }}
                                variant="contained"
                                size="large"
                                startIcon={<SearchIcon />}
                            >
                                Arama
                            </Button>
                        </div>

                    </div>

                </div>
            </div>
            <div className="container" style={{ marginTop: 85 }}>
                <div style={{ display: 'flex', flexDirection: 'row' }}>
                    <h1 style={
                        {
                            left: "calc(50% - 272px/2 - 463px)",
                            fontSize: "35px",
                            fontWeight: 'bold',
                            flex: 1
                        }}>Popüler Filimler</h1>
                    <a href={'#'} style={{ color: '#F5C518' }}>Daha fazla göster -></a>

                </div>

                {data.map((item, key) => <ImdbCard key={key}  {...item} />)}


            </div>
        </div>
    )
}

IMDB.getInitialProps = async ({ req }) => {
    const res = await fetch('https://api.themoviedb.org/3/movie/popular?api_key=844dba0bfd8f3a4f3799f6130ef9e335&language=en-US&page=1')
    const json = await res.json()
    return { best: json }
}


export default IMDB
