import Head from 'next/head';
import React from 'react';
import Header from '../../components/layout/header';
import Movies from '../../components/series';
const Series = (props) => {
  return (
    <>
      <Head>
        <title>Diziler</title>
      </Head>
      <Header />
      <Movies {...props} />
    </>
  )
}

Series.getInitialProps = async (ctx) => {
  const res = await fetch('http://emreramazanoglu.com/film/index.php/series')
  const json = await res.json()

  return { series: json }
}


export default Series;