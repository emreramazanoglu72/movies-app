import Link from 'next/link';
import React from 'react';
import { Player } from 'video-react';
import Header from '../../../components/layout/header';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/FavoriteSharp';
import Head from 'next/head';

const Movies = (props) => {

    return (
        <>
            <Head>
                <title>{props.detail.title}</title>
            </Head>
            <Header />
            <div className="single-page-agile-main">
                <div className="container">
                    {/* /w3l-medile-movies-grids */}
                    <div className="agileits-single-top">
                        <ol className="breadcrumb">
                            <li>
                                <Link href="/">
                                    <a href="/">Ana Sayfa</a>
                                </Link>
                            </li>
                            <li>diziler</li>
                            <li className="active">{props.detail.title}</li>
                        </ol>
                    </div>
                    <div className="single-page-agile-info">
                        {/* /movie-browse-agile */}
                        <div className="show-top-grids-w3lagile">
                            <div className="col-sm-8 single-left">
                                <div className="song">
                                    <div className="song-info" style={{ display: 'flex', flexDirection: 'row' }}>
                                        <h3 style={{ flex: 1 }}>{props.detail.title}</h3>
                                        <IconButton color="secondary" aria-label="delete">
                                            <DeleteIcon color={'green'} />
                                        </IconButton>
                                    </div>
                                    <div className="video-grid-single-page-agileits">
                                        {/* <iframe width={750} height={500} src={props.detail.video_url} frameBorder={0} allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowFullScreen /> */}
                                        <Player

                                            videoId="video-1"
                                        >
                                            <source src={props.detail.video_url} />
                                        </Player>
                                    </div>
                                    <p style={{ fontSize: 16, fontWeight: 'bold' }} dangerouslySetInnerHTML={{ __html: props.detail.description }} ></p>
                                </div>

                                <div className="clearfix"> </div>

                            </div>
                            <div className="col-md-4 single-right">
                                <h3>Up Next</h3>
                                <div className="single-grid-right">

                                    {props.smilar.map((item,key) => <div key={key} className="single-right-grids">
                                        <div className="">
                                            <Link href={'/movies/[key]'}
                                                as={`/movies/${item.movies_seflink}`} prefetch={false}><a ><img src={item.movies_images} style={{ height: 200 }} alt /></a></Link>
                                        </div>
                                        <div className="">
                                            <Link href={'/movies/[key]'}
                                                as={`/movies/${item.movies_seflink}`} prefetch={false}><a className="title"> {item.movies_title}</a></Link>
                                            <p className="author"><a href="#" className="author">John Maniya</a></p>
                                            <p className="views">2,114,200 views</p>
                                        </div>
                                        <div className="clearfix"> </div>
                                    </div>)}


                                </div>
                            </div>
                            <div className="clearfix"> </div>
                        </div>

                    </div>
                </div>
            </div>
        </>

    )
}
Movies.getInitialProps = async (ctx) => {
    const res = await fetch(`http://emreramazanoglu.com/film/index.php/series_detail/${ctx.query.seflink}`)
    const json = await res.json()

    const smilar = await fetch(`http://emreramazanoglu.com/film/index.php/benzer`)
    const json2 = await smilar.json()
    return { detail: json, smilar: json2 }
}

export default Movies;