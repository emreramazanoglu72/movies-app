import Head from 'next/head';
import React from 'react';
import Header from '../../components/layout/header';
import SeriesList from '../../components/seriesList';
const Series = (props) => {
  return (
    <>
      <Head>
        <title>Diziler</title>
      </Head>
      <Header />
      <SeriesList {...props} />
    </>
  )
}

Series.getInitialProps = async (ctx) => {
  const res = await fetch(`http://emreramazanoglu.com/film/index.php/series_list/${ctx.query.seflink}`)
  const json = await res.json()

  return { detail: json }
}


export default Series;