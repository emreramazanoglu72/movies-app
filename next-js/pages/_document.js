import Document, { Html, Head, Main, NextScript } from 'next/document'

class MyDocument extends Document {
    static async getInitialProps(ctx) {
        const initialProps = await Document.getInitialProps(ctx)
        return { ...initialProps }
    }

    render() {
        return (
            <Html data-theme="dark">
                <Head />
                <base href="http://localhost:3000/"/>
                <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
                <link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
                <link rel="stylesheet" href="css/contactstyle.css" type="text/css" media="all" />
                <link rel="stylesheet" href="css/faqstyle.css" type="text/css" media="all" />
                <link href="css/single.css" rel='stylesheet' type='text/css' />
                <link href="css/medile.css" rel='stylesheet' type='text/css' />
                <link href="css/jquery.slidey.min.css" rel="stylesheet" />
                <link href="css/popuo-box.css" rel="stylesheet" type="text/css" media="all" />
                <link rel="stylesheet" href="css/font-awesome.min.css" />
                <script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
                <link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,700italic,700,400italic,300italic,300'
                    rel='stylesheet' type='text/css' />
                <script type="text/javascript" src="js/move-top.js"></script>
                <script type="text/javascript" src="js/easing.js"></script>
                <body>
                    <Main />
                    <NextScript />
                    <script src="js/jquery.slidey.js"></script>
	<script src="js/jquery.dotdotdot.min.js"></script>
                    <script src="js/bootstrap.min.js"></script>

                </body>
            </Html>
        )
    }
}

export default MyDocument