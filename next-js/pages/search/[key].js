import React, { useEffect, useState } from 'react';
import Header from '../../components/layout/header';
import Search from './search'
const Series = (props) => {


    return (
        <div>
            <Header />
            <Search {...props} />
        </div>
    )
}

Series.getInitialProps = async (ctx) => {
    const res = await fetch(`http://emreramazanoglu.com/film/index.php/search/${ctx.query.key}`)
    const json = await res.json()

    return { search: json }
}


export default Series;