import React, { useEffect, useState } from 'react';
import Header from '../../components/layout/header';
import CardList from '../../components/cardList';
import IMDBFavority from '../../components/imdbFavority';


const Series = (props) => {
  var [data, setData] = useState([]);
  var [imdb, setimdb] = useState([]);
  useEffect(() => {
    props.movies == '' ? setData(JSON.parse(localStorage.getItem("favority"))) : setData(props.movies)
    props.IMDB == '' ? setimdb(JSON.parse(localStorage.getItem("IMDB"))) : setimdb(props.IMDB)
  }, [])
  return (
    <div>
      <Header />
      
      {props.IMDB == "" ? null : <IMDBFavority imdb={imdb} />}
      {props.movies == "" ? <div style={{ textAlign: 'center', marginTop: 25 }} >
        <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTbnTCjL_7-pIzDQg2W30Vy1wdTNuy8zYAP8A&usqp=CAU" alt="" />
        <h1>Henüz Favorilerde bir şey bulamadık</h1>
      </div> : <CardList movies={data} />}
    </div>
  )
}

Series.getInitialProps = async ({ req }) => {
  if (req) {
    return { movies: [] }
  } else {
    const res = JSON.parse(localStorage.getItem("favority"))
    const IMDB = JSON.parse(localStorage.getItem("IMDB"))
    return { movies: res ,IMDB:IMDB}
  }

}


export default Series;