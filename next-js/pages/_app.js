import "video-react/dist/video-react.css"; // import css
import 'nprogress/nprogress.css'; //styles of nprogress
import "react-multi-carousel/lib/styles.css";

import Router from 'next/router';
//Binding events. 
import NProgress from 'nprogress'; //nprogress module
import { useEffect } from "react";

Router.events.on('routeChangeStart', () => NProgress.start());
Router.events.on('routeChangeComplete', () => NProgress.done());
Router.events.on('routeChangeError', () => NProgress.done());
function MyApp({ Component, pageProps }) {
  useEffect(() => {
    localStorage.getItem('favority') == null ? localStorage.setItem('favority', JSON.stringify([])) : null
    localStorage.getItem('IMDB') == null ? localStorage.setItem('IMDB', JSON.stringify([])) : null

  });
  return <Component {...pageProps} />
}


export default MyApp